# Prevention of Sexual Harassment at the Workplace(POSH)
## Sexual Harassment Behaviors:
Sexual harassment can encompass a range of unwelcome behaviors that are sexual in nature. Here are some common examples:

### Verbal: 
Sexual comments, jokes, innuendo, threats, or pressure for dates.
### Non-verbal:
 Leering, staring, unwanted touching, sexual gestures, or following someone.
### Visual: 
Displaying offensive pictures or videos.
Remember: Even if not intended to offend, any behavior that makes someone feel uncomfortable or unsafe sexually can be considered harassment.
### Quid pro quo sexual harassment
One party forces the other party to offer sex in return for recruitment, promotion or salary
raise within the first party's powers, and threatens to demote, cut the salary or even fire
the second party if rejected 

## Facing or Witnessing Sexual Harassment:
Here's what you can do if you face or witness sexual harassment:

- `If you are being harassed`:
Speak up firmly and clearly: Tell the person to stop their behavior and that you find it offensive.
- `Document the incident`: Keep a record of the date, time, location, details of the incident, and any witnesses.
Report the incident: Report it to a supervisor, Human Resources department, or a trusted person who can help. There might be anonymous reporting options available as well.
- `Seek support`: Talk to a friend, family member, counselor, or crisis hotline for emotional support and guidance.
## If you witness harassment:
- `Speak up if it's safe to do so`: Support the person being harassed and let the perpetrator know their behavior is inappropriate.
- `Report the incident`: Report it anonymously or directly to a person in authority who can take action.
- `Offer support`: Offer support and resources to the person being harassed.
- `Delegate`: If you can't intervene directly in something because there is a barrier that makes you uncomfortable, then enlist some help. Ask friends to assist you or talk to a faculty or staff member. Maybe it means you need to call the police.
### Here are some ways to report sexual harassment:
- Call the National Sexual Assault Hotline: 800.656. HOPE (4673)
- Chat online: online.rainn.org
- Lodge a complaint online with the National Commission for Women: Fill out the complaint form with details of complainant or victim such as name, address, contact numbers, date of birth and respondent detail