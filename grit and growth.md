# Grit and Growth Mindset
## What is Grit ?
- Grit means not giving up and sticking to our passions by working hard on it to achieve success.
- By being gritty we can face failures and lean from them.

## Growth Mindset
- Growth mindset is a concept that promotes effort and practice for skill development.
-  People with a growth mindset tend to learn, grow, and achieve more over time than people with a fixed mindset. 
- People with a fixed mindset avoid the four key ingredients to growth, which are effort, challenges, mistakes, and feedback.

## What is the Internal Locus of Control? What is the key point in the video?
- Solving problems and appreciating their actions can help adopt an internal locus of control
- Locus of control is the degree to which one believes they have control over their life

## What are the key points mentioned by speaker to build growth mindset?
The key points mentioned by speaker are:
- Fixed mindset vs. growth mindset
- Believing your self for solving the problems
- seek feedback and correct your self from the feedback
- Create your own timetable for doings things

## What are your ideas to take action and build Growth Mindset?
- Embrace Challenges
- Learn from Failure
- Set Goals
- Seek Feedback
- Reflect Regularly
- Understand the concepts properly