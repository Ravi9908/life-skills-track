# Listening and Active Communication

## What are the steps/strategies to do Active Listening?
- Focus on the speaker and what he is saying.
- Avoid getting distracted.
- Use some body language gestures showing that you are listening.
- Takes notes in your own words of important details.
  
## According to Fisher's model, what are the key points of Reflective Listening? 
- Reflective listening means listening to the speaker attentively observing the the way of his talking like his expressions and responding according to that.
  ### Key points of Fisher;s model:
  - actively listening to the speaker.
  - Make him feel like he can talk freely.
  - Go with mood of the speaker.
  - Make your own words speech from the sepaker speech.
## What are the obstacles in your listening process?
- Lack of interest.
- Disturbing noises from surroundings.
- Fear of the complexity in topic.
- Electronic Devices.
## What can you do to improve your listening?
- Making the interest in topic by getting info about it by ourselves.
- Takes a place where there has no noise or less noise
- When topic is complex take a notes and by doing research break down the complex task into simple problems.
- When in meeting or listening to important speech 
  stop using electronic devices that time until you understand the topic of the speaker.
- We can also take notes while listening to not miss any details.
- We can create a our own words speech about the meeting and share it with others to get checked it.

## When do you switch to Passive communication style in your day to day life?
-  To Avoiding Conflict
- Fear of Upsetting Others
- Low Belief in myself
- To maintain harmony
- To respect the other person
## When do you switch into Aggressive communication styles in your day to day life?
  - Feeling Disrespected
  - Feeling Threatened
  - Feeling Frustrated
  - Under pressure
## When do you switch into Passive Aggressive communication styles in your day to day life?
- Ignoring my opinions or feelings
- Talking in a threatened or hurtful way
  
## How can you make your communication assertive?  what would be a few steps you can apply in your own life?
- Be clear and direct
- Be confident when speaking
- Be respectful
- Focus on the lsiteing on the speaker
- Compromise in some cases 



 