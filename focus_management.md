# Focus Management

## What is Deep Work?
  - Deep Work is a term coined by Cal Newport in his book  Good They Can't Ignore You to refer to professional activities performed in a state of distraction-free concentration that push your cognitive capabilities to their limit. 
  - These efforts create new value, improve your skill, and are hard to replicate.
## According to author how to do deep work properly, in a few points?
- Identify Your Deep Work Sweet Spot
- Craft a Deep Work Environment
- Batch Similar Tasks Together
- Embrace the Power of Breaks


## How can you implement the principles in your day to day life?
- Schedule your distraction periods
- Develop a rhythmic deep work ritual
- Have a daily shutdown complete ritual
- Embrace Focused Distraction

## What are the dangers of social media, in brief?

- Social media can be addictive and can negatively affect your ability to concentrate.
- social media is designed to be addictive and fragmentation of attention can permanently reduce your capacity for concentration. 
- This can be a major disadvantage in a competitive economy that requires deep work.
- Social media can lead to feelings of inadequacy and loneliness.
-  The constant exposure to people's carefully curated positive portrayals of their lives can make you feel inadequate and increase rates of depression.
- Social media can cause anxiety. 
- Our brains are not wired to be constantly exposed to stimuli with intermittent rewards. This can lead to a pervasive background hum of anxiety.