# Event Sourcing
## What is Event sourcing?

- Event Sourcing is an architectural design pattern that stores data in an append-only log.
- It keeps track of the state of the activity of an event or an application and also stores the changes happening to it in an event log file. 
- Whenever the state of a business entity changes, a new event is appended to the list of events.
- This process is immutable
- Overall, Event Sourcing Architecture is a powerful pattern that can be used to build scalable and auditable applications. However, it is important to be aware of the drawbacks before using it.
## When to use this?
- It will be used when you want the capture the intent, purpose, or reason in the data
- It is used to avoid or minimize the conflict of the updated data.
- When you want the flexibility to be able to change the format of materialized models and entity data if requirements change, or—when used with CQRS(Command Query Responsibility Segregation)—you need to adapt a read model or the views that expose the data.
  
## Key Features
 ### Complete Rebuild
 - We can rebuild the application from the start based on the data stored in the event storage.
 ### Temporal Query
 - We can check the status of the previous event that happened at the specific change or day.
 ### Event Replay
 - An entity's current state can be created by replaying all the events in order of occurrence.
   
## Pros
- Audit purpose
- Parallel Processing
- Reproducibility:
- scalable
## Cons
- Complexity
- Performance
- Cost
## Some Examples
### Order processing:
An order processing system could use Event Sourcing Architecture to store the history of changes to an order. This would allow the system to be audited and to be reproduced if necessary.
### Customer relationship management (CRM):
A CRM system could use Event Sourcing Architecture to store the history of interactions with customers. This would allow the system to provide a personalized experience for each customer.


## Event Sourcing Flow chart
![EVentsourcing Flow chart  ](https://learn.microsoft.com/en-us/azure/architecture/patterns/_images/event-sourcing-overview.png)

# Reference
[Event sourcing Data collected](https://learn.microsoft.com/en-us/azure/architecture/patterns/event-sourcing)

[Event sourcing Image](https://learn.microsoft.com/en-us/azure/architecture/patterns/event-sourcing)

