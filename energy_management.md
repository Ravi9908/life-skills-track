# Energy Management
## What are the activities you do that make you relax - Calm quadrant?
The activities that i make  myself to feel calm are:
- Some breathing in tense situation.
- Do some walking .

## When do you find getting into the Stress quadrant?
- Getting near to deadlines.
- To have work in limited or less time.
- TO be in unlikable situation.
  
## How do you understand if you are in the Excitement quadrant?
- When getting or doing something you like.
- When some work gives high satisfaction .
- When the thing you are doing is fun.
## Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Lack of sleep affects memory and learning abilities.
- Sleep is important for memory consolidation.
- poor sleep can cause health problems
- Helps to the immune system.
- Short sleep can leads to cancer.
- Sleep loss disrupts gene activity.

## What are some ideas that you can implement to sleep better?
- Maintain regularity on sleep on same time daily.
- Keep bedroom cool or comfortable for sleeping.
- Keep electronic devices away before going to bed.

## Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
-  It has immediate, long-lasting, and protective benefits.
- It increases the levels of neurotransmitters like dopamine, serotonin, and noradrenaline, which improves mood, focus, attention, and reaction times.
Exercise increases the size of the hippocampus, which is critical for long-term memory.
- Exercise protects the brain from neurodegenerative diseases like Alzheimer's disease and dementia.
- The minimum amount of exercise recommended is 3-4 times a week for 30 minutes each time.
-  You can get aerobic exercise by walking, taking the stairs, or even power vacuuming. 

## What are some steps you can take to exercise more?
- The rule of thumb is to get at least 30 minutes of aerobic exercise three to four times a week. 
- You can break it down into smaller chunks of time throughout the day if that works better for you.
- Aerobic exercise means getting your heart rate up. You can do this by brisk walking, running, swimming or biking.
- You don't need a gym membership to get exercise. You can find ways to incorporate exercise into your daily routine such as taking the stairs instead of the elevator or doing some jumping jacks during commercial breaks while watching TV. 