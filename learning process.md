# Learning Process
## What is the Feynman Technique?
- if you want to learn something complex you have to understand it simply and teach or explain it to others for your deeper understanding. 
- it has four steps:
  - Writing down the concept, 
  - Explaining it in plain English
  - Identifying any areas of confusion 
  - Trying to explain it in simplified language 
## In this video, what was the most interesting story or idea for you?

- The most interesting idea for me was Dr. Oakley's explanation of the two modes of learning: 
  - `The focused mode or Active mode` 
  - `The diffuse mode` 
- It's fascinating to think about how our brains can switch between these states to facilitate different aspects of learning and problem-solving. 
- The pinball machine analogy she used to illustrate this concept was particularly good in conveying how thoughts move between these modes. 
- It shows the importance of embracing both modes to become more effective learners.

## What are active and diffused modes of thinking?

- `Active mode` of thinking is when you're focused on a specific task, like studying or solving a problem.
- `Diffused mode` of thinking is when your mind is more relaxed, allowing for creativity and making connections between ideas.

## What are the steps to take when approaching a new topic?
 - Identify key concepts and principles.
 - Focus on deliberate practice and breaking down skills into manageable chunks.
 - Breaking down skills into manageable chunks to make the learning process more accessible and enjoyable.
 - Seek feedback and adjust learning strategies as needed.
 - Stay persistent and patient throughout the learning process.

## What are some of the actions you can take going forward to improve your learning process?
- Practice repetition to reinforce learning over time
- Stay organized and manage your time effectively to maximize productivity.
- Take good rest for your brain to function healthily
- Take notes of mistakes or concepts that are hard to understand search for simpler ways to understand and practice repeatedly
- Do practice more on the harder concepts to gain a better understanding. 
